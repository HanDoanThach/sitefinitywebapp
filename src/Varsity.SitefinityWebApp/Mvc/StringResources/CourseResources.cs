﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Localization;

namespace Varsity.SitefinityWebApp.Mvc.StringResources
{
    [ObjectInfo(typeof(CourseResources),
       ResourceClassId = "CourseResources",
       Title = "CourseResourcesTitle",
       Description = "CourseResourcesDescription")]
    public class CourseResources : Resource
    {
        [ResourceEntry("CourseResourcesTitle",
            Value = " Course Resources labels",
            Description = "The title of this class",
            LastModified = "2019/11/01")]
        public string CourseResourcesTitle
        {
            get
            {
                return this["CourseResourcesTitle"];
            }
        }

        /// <summary>
        /// Full path to the current page
        /// </summary>
        [ResourceEntry("CourseResourcesDescription",
            Value = "Contains localizable resources",
            Description = "The description of this class",
            LastModified = "2019/11/01")]
        public string CourseResourcesDescription
        {
            get
            {
                return this["CourseResourcesDescription"];
            }
        }
    }
}