﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Sitefinity.Localization;

namespace Varsity.SitefinityWebApp.Mvc.StringResources
{
    [ObjectInfo(typeof(ExampleModuleResources),
       ResourceClassId = "ExampleModuleResources",
       Title = "ExampleModuleResourcesTitle",
       Description = "ExampleModuleResourcesDescription")]
    public class ExampleModuleResources : Resource
    {
        [ResourceEntry("ExampleModuleResourcesTitle",
            Value = " ExampleModule Resources labels",
            Description = "The title of this class",
            LastModified = "2019/11/01")]
        public string ExampleModuleResourcesTitle
        {
            get
            {
                return this["ExampleModuleResourcesTitle"];
            }
        }

        /// <summary>
        /// Full path to the current page
        /// </summary>
        [ResourceEntry("ExampleModuleResourcesDescription",
            Value = "Contains localizable resources",
            Description = "The description of this class",
            LastModified = "2019/11/01")]
        public string ExampleModuleResourcesDescription
        {
            get
            {
                return this["ExampleModuleResourcesDescription"];
            }
        }
    }
}