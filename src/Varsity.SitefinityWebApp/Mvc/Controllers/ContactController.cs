﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity;
using Telerik.Sitefinity.Data;
using Telerik.Sitefinity.DynamicModules;
using Telerik.Sitefinity.DynamicModules.Model;
using Telerik.Sitefinity.Model;
using Telerik.Sitefinity.Mvc;
using Telerik.Sitefinity.Security;
using Telerik.Sitefinity.Utilities.TypeConverters;
using Telerik.Sitefinity.Versioning;
using Varsity.SitefinityWebApp.Mvc.Models;
using Varsity.SitefinityWebApp.Utility;

namespace Varsity.SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Contact_MVC", Title = "Contact", SectionName = "CustomWidgets")]
    public class ContactController : Controller
    {
        public ActionResult Index()
        {
            var viewModel = new ContactPageViewModel()
            {
                Title = this.Title,
                Summary = this.Summary,
                IFrameMap = this.IFrameMap
            };
            return View(viewModel);
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ContactViewModel viewModel)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    CreateContact(viewModel);
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return HttpNotFound();
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(ContactViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    UpdateContact(viewModel);
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return HttpNotFound();
            }
        }

        public ActionResult Delete(int id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Delete(ContactViewModel viewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    DeleteContact(viewModel);
                    return RedirectToAction("Index");
                }
                return HttpNotFound();
            }
            catch
            {
                return HttpNotFound();
            }
        }

        private void CreateContact(ContactViewModel viewModel)
        {
            var providerName = string.Empty;
            var cultureName = SystemConstant.CultureNameEN;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
            Type contactType = TypeResolutionService.ResolveType(SystemConstant.ContactType);
            DynamicContent contactItem = dynamicModuleManager.CreateDataItem(contactType);

            CreateContactItem(dynamicModuleManager, contactItem, viewModel);
        }


        private void UpdateContact(ContactViewModel viewModel)
        {
            var contact = GetContactById(viewModel.ContactId);
            if(contact != null)
            {
                var providerName = string.Empty;
                var cultureName = SystemConstant.CultureNameEN;
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
                CreateContactItem(dynamicModuleManager, contact, viewModel);
            }
        }

        private void DeleteContact(ContactViewModel viewModel)
        {
            var contact = GetContactById(viewModel.ContactId);
            if (contact != null)
            {
                var providerName = string.Empty;
                var cultureName = SystemConstant.CultureNameEN;

                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);
                DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
                dynamicModuleManager.DeleteDataItem(contact);
                using (new ElevatedModeRegion(dynamicModuleManager))
                {
                    dynamicModuleManager.SaveChanges();
                }
            }
        }

        public DynamicContent GetContactById(Guid contactId)
        {
            var providerName = string.Empty;

            DynamicModuleManager dynamicModuleManager = DynamicModuleManager.GetManager(providerName);
            Type examplemoduleType = TypeResolutionService.ResolveType(SystemConstant.ContactType);

            return dynamicModuleManager.GetDataItems(examplemoduleType).FirstOrDefault(em => em.Id == contactId);
        }


        private void CreateContactItem(DynamicModuleManager dynamicModuleManager, DynamicContent contactItem, ContactViewModel viewModel)
        {
            var cultureName = SystemConstant.CultureNameEN;
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureName);

            contactItem.SetString("name", viewModel.Name, cultureName);
            contactItem.SetString("message", viewModel.Message, cultureName);
            contactItem.SetString("email", viewModel.Email, cultureName);
            contactItem.SetString("subject", viewModel.Subject, cultureName);


            contactItem.SetString("UrlName", FriendlyUrlHelper.GetFriendlyTitle(string.Format("{0} {1}","contact",viewModel.Name), true), cultureName);
            contactItem.SetValue("PublicationDate", DateTime.UtcNow);


            contactItem.SetWorkflowStatus(dynamicModuleManager.Provider.ApplicationName, WorkFlowStatus.Draft, new CultureInfo(cultureName));

            var versionManager = VersionManager.GetManager();
            versionManager.CreateVersion(contactItem, false);
            using (new ElevatedModeRegion(dynamicModuleManager))
            {
                dynamicModuleManager.SaveChanges();
            }

            DynamicContent checkOutContactItem = dynamicModuleManager.Lifecycle.CheckOut(contactItem) as DynamicContent;
            DynamicContent checkInContactItem = dynamicModuleManager.Lifecycle.CheckIn(checkOutContactItem) as DynamicContent;
            versionManager.CreateVersion(checkInContactItem, false);
            using (new ElevatedModeRegion(dynamicModuleManager))
            {
                dynamicModuleManager.SaveChanges();
            }
        }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string IFrameMap { get; set; }
    }
}
