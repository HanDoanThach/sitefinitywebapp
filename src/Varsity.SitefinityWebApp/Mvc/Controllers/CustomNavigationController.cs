﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Telerik.Sitefinity.Mvc;

namespace Varsity.SitefinityWebApp.Mvc.Controllers
{
    [ControllerToolboxItem(Name = "Navigation_MVC", Title = "Navigation", SectionName = "CustomWidgets")]
    public class CustomNavigationController : Telerik.Sitefinity.Frontend.Navigation.Mvc.Controllers.NavigationController
    {

    }
}