﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Varsity.SitefinityWebApp.Mvc.Models
{
    public class ExampleModuleViewModel
    {
        public Guid ExampleId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public DateTime Birthday { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }
        [Required]
        public Guid CountryId { get; set; }
        public IList<CountryViewModel> Countries { get; set; }
    }

    public class CountryViewModel
    {
        public Guid CountryId { get; set; }
        public string Title { get; set; }
    }

    public class ExampleModulePageViewModel
    {
        public int PageSize { get; set; }
        public IList<ExampleModuleViewModel> ExampleModuleViewModels { get; set; }
    }
}