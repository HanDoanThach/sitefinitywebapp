using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Telerik.Sitefinity.Abstractions;
using Telerik.Sitefinity.Configuration;
using Telerik.Sitefinity.Localization;
using Varsity.SitefinityWebApp.Mvc.StringResources;

namespace Varsity.SitefinityWebApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Bootstrapper.Bootstrapped += Bootstrapper_Bootstrapped;
        }

        private void Bootstrapper_Bootstrapped(object sender, EventArgs e)
        {
            Res.RegisterResource<CustomBreadcrumbResources>();
            Res.RegisterResource<CustomNavigationResources>();
            Res.RegisterResource<CourseResources>();
            Res.RegisterResource<ContactResources>();
            Res.RegisterResource<ExampleModuleResources>();
        }
    }
}
