﻿using System.Web;
using System.Web.Optimization;

namespace Varsity.SitefinityWebApp
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.unobtrusive*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "jquery.min.js"));                      


            bundles.Add(new StyleBundle("~/Content/css/site").Include(                      
                      "~/Content/css/bootstrap.css",
                      "~/Content/css/font-awesome.css",
                      "~/Content/css/font.css",
                      "~/Content/css/Site.css",
                       "~/Content/css/bootstrap.min.css",
                       "~/Content/css/Home2.css",                     
                      "~/Content/css/select2.css"));

            bundles.Add(new ScriptBundle("~/bundles/datePicker").Include(
                 "~/Scripts/moment.js",   
                 "~/Scripts/bootstrap-datepicker.js",
                 "~/Scripts/bootstrap-datepicker.min.js",
                  "~/Scripts/bootstrap.js",
                  "~/Scripts/bootstrap-datepicker.vi.min.js"));

            bundles.Add(new StyleBundle("~/bundles/css/datePicker").Include(
                  "~/Content/css/bootstrap-datepicker.css",
                  "~/Content/css/bootstrap-datepicker.min.css"));

            bundles.Add(new StyleBundle("~/Content/pagedList").Include(
                      "~/Content/css/PagedList.css"));

        }
    }
}
