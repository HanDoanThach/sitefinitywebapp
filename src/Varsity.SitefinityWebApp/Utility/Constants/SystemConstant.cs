﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Varsity.SitefinityWebApp.Utility
{
    public static class SystemConstant
    {
        public static string ExampleTransaction = "Example Transaction";
        public static string CultureNameEN = "en";
        public static string ExampleModuleType = "Telerik.Sitefinity.DynamicTypes.Model.ExampleModules.Examplemodule";
        public static string ContactType = "Telerik.Sitefinity.DynamicTypes.Model.Contacts.Contact";
        public static string ApplicationName = "/DynamicModule";
        public static string TaxonomyCountry = "countries";
    }

    public static class WorkFlowStatus
    {
        public static string Draft = "Draft";
        public static string Published = "Published";
    }

}